package fi.dy.janska.htov2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Janne Summanen on 18.11.2014.
 */
public class fetchImg extends AsyncTask<ZoomableImageView,Void,Bitmap> {
    private ZoomableImageView iv;
    private boolean LogEnabled= false;
    private static final String TAG = "LoadIMG";
    private static final String osoite = "http://www.htory.fi/Dokumentit/toimistokamera/webcam.jpg";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Bitmap doInBackground(ZoomableImageView... views) {
        this.iv = views[0];
        try {
            log('d', "Aloitetaan kuvan haku");
            URL url = new URL(osoite);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            log('d', "Avataan yhteys");
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            input.close();
            log('d', "Lataus onnistui");
            return myBitmap;
        } catch (IOException e) {
            log('e', "Kuvan lataus epäonnistui, IOException");
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap s) {
        super.onPostExecute(s);
        try {
            if (s != null) {
                log('d', "Bitmap ei null asetetaan kuvaksi");
                iv.setImageBitmap(s);
            } else {
                log('d', "Bitmap null asetetaan kuvaksi ei oleee");
                iv.setImageResource(R.drawable.webcam);
            }
        } catch (Exception e) {
            log('e', "Image download failed");
        }
    }

    /*
    *   Keskitetty Logitus
     */
    private void log(char lvl, String msg) {
        if (!LogEnabled) { return; }
        switch (lvl) {
            case 'e':
                Log.e(TAG, msg);
                break;
            case 'd':
                Log.d(TAG, msg);
                break;
            case 'i':
                Log.i(TAG, msg);
                break;
        }
    }

}
