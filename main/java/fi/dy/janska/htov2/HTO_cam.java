package fi.dy.janska.htov2;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;

/**
 * Created by Janne Summanen
 */

public class HTO_cam extends Activity {
    private ZoomableImageView kuva;
    private Handler handler = new Handler();
    private boolean nakyvissa;
    private final String TAG = "Main";

    private Runnable ajettava = new Runnable() {

        @Override
        public void run() {
            if (nakyvissa) {
                new fetchImg().execute(kuva);
            }
            handler.postDelayed(ajettava, 10000); // Toista 10 000ms välein
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hto_cam);
        kuva = (ZoomableImageView) findViewById(R.id.ivKuva);
        //kuva.setOnTouchListener(kuva);
    }
    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(ajettava, 200);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nakyvissa = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        nakyvissa = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(ajettava);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
